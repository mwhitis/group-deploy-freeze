from distutils.sysconfig import project_base
import argparse
import requests
from envparse import Env
import os
from urllib.parse import quote_plus
from gitlab import Gitlab


env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')
cron_time_zone = env('CRON_TIME_ZONE')

# GitLab API variables
api_token = env('ACCESS_TOKEN')
group_slug = env('GROUP_SLUG')
gitlab_origin = env('GITLAB_ORIGIN', default='https://gitlab.com')
base_url = f'{gitlab_origin}/api/v4'
gl = Gitlab(gitlab_origin, api_token)
gl.auth()

# Define the headers with the authorization token
headers = {
    'Authorization': f'Bearer {api_token}'
}

def create_freeze_period(project_id, freeze_periods):
    freeze_url = f'{base_url}/projects/{project_id}/freeze_periods'
    for freeze_start, freeze_end in freeze_periods:
        data = {
            'freeze_start': freeze_start,
            'freeze_end': freeze_end,
            'cron_timezone': cron_time_zone,
            'protected_branches': ['master']  # Adjust the branch name as needed
        }
        response = requests.post(freeze_url, headers=headers, json=data)

        # Check if creating the freeze period was successful
        if response.status_code == 201:
            print(f"Freeze period created for project ID '{project_id}'")
        else:
            print(f"Failed to create freeze period for project ID '{project_id}': {response.json()}")


def delete_freeze_periods(project_id):
    try:
        # Retrieve all freeze periods for the project
        freeze_url = f'{base_url}/projects/{project_id}/freeze_periods'
        response = requests.get(freeze_url, headers=headers)
        if response.status_code == 200:
            freeze_periods = response.json()
            # Delete each freeze period
            for freeze_period in freeze_periods:
                freeze_period_id = freeze_period['id']
                delete_url = f'{base_url}/projects/{project_id}/freeze_periods/{freeze_period_id}'
                delete_response = requests.delete(delete_url, headers=headers)
                if delete_response.status_code == 204:
                    print(f"Deleted freeze period with ID '{freeze_period_id}' for project ID '{project_id}'.")
                else:
                    print(f"Failed to delete freeze period with ID '{freeze_period_id}' for project ID '{project_id}': {delete_response.json()}")
        else:
            print(f"Failed to retrieve freeze periods for project ID '{project_id}': {response.json()}")

    except Exception as e:
        print(f"An error occurred while processing freeze periods for project ID '{project_id}': {str(e)}")
        exit(1)


def fetch_projects(group_id):
    """
    Given a group id, identify all of the projects by ID and return
    that as a list of project IDs. This will iterate through all
    of a group's sub-groups as well, gathering their projects.
    """

    group = gl.groups.get(group_id)
    projects = group.projects.list(include_subgroups=True)

    project_ids = []
    for project in projects:
        project_ids.append(project.id)

    return project_ids


def main():

    freeze_start = env('FREEZE_START', '')
    freeze_end = env('FREEZE_END', '')
    if not freeze_start or not freeze_end:
        print("Error: Freeze Start ({freeze_start}) or Freeze End ({freeze_end}) has not been defined.")
        exit(1)

    start_periods = freeze_start.split('\n')
    end_periods = freeze_end.split('\n')
    if len(start_periods) != len(end_periods):
        print(f"Error: The number of freeze start periods ({len(start_periods)}) and freeze end periods ({len(end_periods)}) do not match.")
        exit(2)

    # Retrieve the freeze periods from the environment variables
    # and "repackage" them so that each corresponding start/end 
    # are together in a list of lists.
    freeze_periods = []
    for start, end in zip(start_periods, end_periods):
        freeze_periods.append((start, end))

    # Retrieve the group details based on the provided group slug    
    encoded_group_slug = quote_plus(group_slug)

    group_url = f'{base_url}/groups/{encoded_group_slug}'
    response = requests.get(group_url, headers=headers)
    group_data = response.json()

    if 'id' not in group_data:
        print(f"Failed to retrieve group ID for group slug '{group_slug}': {group_data}")
        exit(3)

    group_id = group_data['id']
    print(f"Retrieved group ID '{group_id}' for group slug '{group_slug}'")

    # Fetch all projects (including subgroups)
    project_ids = fetch_projects(group_id)

    # Print the extracted project IDs
    print("Extracted Project IDs:")
    print(project_ids)

    # Loop through project IDs and delete existing deploy freezes
    for project_id in project_ids:
        delete_freeze_periods(project_id)
        create_freeze_period(project_id, freeze_periods)


if __name__ == '__main__':
    main()

