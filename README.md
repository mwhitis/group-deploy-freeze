# freezeperiods


## Getting Started.

This project shows how to use the GitLab API to automatically set
freeze periods for all projects within a specific group.

It is coded for GitLab.com but can be leveraged within a GitLab Self-Managed 
Installation as well. 

## Other Notes

Work done in conjunction with providing a new Freeze Period Template for the GitLab Docs.

* https://gitlab.com/cnnachi-demo/deploymenttemplate/-/blob/main/deployment.gitlab-ci.yml
* https://gitlab.com/cnnachi-demo/testspringapp 
